=== Toptal Save ===
Contributors: ratkosolaja
Tags: save for later, bookmark, pocket, logged out, wishlist
Requires at least: 3.0
Tested up to: 4.7
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Save content for later.

== Description ==

Created for purposes of Toptal Engineering blog.

**Toptal Save** will add a button to your posts/pages/custom post types so users who aren't logged in as well as ones who are, can save that content so they can access it later. This plugin uses cookies to make this possible. This plugin is light, fast (ajax powered) and coded with best practice.

Upon activation this plugin will create a page called "Saved", and when a user access it, it will show him his saved content. When you deactivate this plugin, that page will be deleted as well.

**Features:**

* You can choose whether you want this plugin to add a save for later button to the end of your content.
* You can pick on which post types you want to show save for later button.
* You can choose whether you want to use our minimal styling or not.
* Included Shortcode

**Live demo can be found over [here](http://ratkosolaja.info/plugins/2016/07/10/possimus-quisquam-animi-omnis-laboriosam/).**

== Installation ==

They are two ways we can go about this: **Using the WordPress Plugin Search** or **Using the WordPress Admin Plugin Upload**.

Using the WordPress Plugin Search:

1. Go to WordPress admin area.
2. Click on Plugins > Add New.
3. In the Search box, type in **Toptal Save** and hit enter.
4. Next to the **Toptal Save** click on the Install Now button.
5. Click on Activate Plugin.

Using the WordPress Admin Plugin Upload:

1. Download the plugin here from our plugin page.
2. Go to WordPress admin area.
3. Click on Plugins > Add New > Upload Plugin.
4. Click on Choose File and select **toptal-save.zip**
5. Click on Install Now button.
6. Click on Activate Plugin.

== Frequently Asked Questions ==

**Caching Plugins**: When using them, please disable caching for the "Saved" page, if you don't it won't show "Saved" content properly.

== Changelog ==

= 1.0.0 =
* Initial Release.